import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import tensorflow as tf


def bias_variable(shape):
    '''
    Initializing bias variables
    '''
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def conv2d(x, W):
    '''
    Perform 2-D convolution with zero-padding
    '''
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def dense_to_one_hot(labels_dense, num_classes):
    '''
    Convert class label to one-hot vector
    Example:
        0 => [1 0 0 0 0 0 0 0 0 0]
        1 => [0 1 0 0 0 0 0 0 0 0]
        ...
        9 => [0 0 0 0 0 0 0 0 0 1]
    '''
    num_labels = labels_dense.shape[0]
    index_offset = np.arange(num_labels) * num_classes
    labels_one_hot = np.zeros((num_labels, num_classes))
    labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
    return labels_one_hot


def max_pool_2x2(x):
    '''
    Perform max pooling with a 2x2 window
    Example:
        [[0,3],
        [4,2]] => 4
    '''
    return tf.nn.max_pool(x,
                          ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1],
                          padding='SAME')


def next_batch(batch_size):

    global train_images
    global train_labels
    global index_in_epoch
    global epochs_completed

    start = index_in_epoch
    index_in_epoch += batch_size

    # when all trainig data have been already used, it is reorder randomly
    if index_in_epoch > num_examples:
        # finished epoch
        epochs_completed += 1
        # shuffle the data
        perm = np.arange(num_examples)
        np.random.shuffle(perm)
        train_images = train_images[perm]
        train_labels = train_labels[perm]
        # start next epoch
        start = 0
        index_in_epoch = batch_size
        assert batch_size <= num_examples
    end = index_in_epoch
    return train_images[start:end], train_labels[start:end]


def weight_variable(shape):
    '''
    Initializing weight variables
    '''
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


LEARNING_RATE = 1e-4
TRAINING_ITERATIONS = 2000  # set to 20000 for 0.99 accuracy
DROPOUT = 0.5
BATCH_SIZE = 50
VALIDATION_SIZE = 2000

# read training data from CSV file
data = pd.read_csv('input/train.csv')
images = data.iloc[:, 1:].values
images = images.astype(np.float)

# convert from [0:255] => [0.0:1.0]
images = np.multiply(images, 1.0 / 255.0)
image_size = images.shape[1]
image_width = image_height = np.ceil(np.sqrt(image_size)).astype(np.uint8)
labels_flat = data[[0]].values.ravel()
labels_count = np.unique(labels_flat).shape[0]
labels = dense_to_one_hot(labels_flat, labels_count)
labels = labels.astype(np.uint8)

# split data into training & validation
validation_images = images[:VALIDATION_SIZE]
validation_labels = labels[:VALIDATION_SIZE]
train_images = images[VALIDATION_SIZE:]
train_labels = labels[VALIDATION_SIZE:]


# Initialize placeholders for rasterized images(x) and labels(y)
x = tf.placeholder('float', shape=[None, image_size])
y_ = tf.placeholder('float', shape=[None, labels_count])

# first convolutional layer
W_conv1 = weight_variable([5, 5, 1, 32])
b_conv1 = bias_variable([32])
image = tf.reshape(x, [-1, image_width, image_height, 1])
h_conv1 = tf.nn.relu(conv2d(image, W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1)

# second convolutional layer
W_conv2 = weight_variable([5, 5, 32, 64])
b_conv2 = bias_variable([64])
h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

# densely connected layer
W_fc1 = weight_variable([7 * 7 * 64, 1024])
b_fc1 = bias_variable([1024])
h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

# dropout
keep_prob = tf.placeholder('float')
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# readout layer for deep net
W_fc2 = weight_variable([1024, labels_count])
b_fc2 = bias_variable([labels_count])
y = tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)

# cost function
cross_entropy = -tf.reduce_sum(y_*tf.log(y))

# optimisation function
train_step = tf.train.AdamOptimizer(LEARNING_RATE).minimize(cross_entropy)

# evaluation
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, 'float'))

# prediction function
predict = tf.argmax(y, 1)
epochs_completed = 0
index_in_epoch = 0
num_examples = train_images.shape[0]

# start TensorFlow session
print('Start TensorFlow')
init = tf.initialize_all_variables()
sess = tf.InteractiveSession()
sess.run(init)

# visualisation variables
train_accuracies = []
validation_accuracies = []
x_range = []

display_step = 1

for i in range(TRAINING_ITERATIONS):
    batch_xs, batch_ys = next_batch(BATCH_SIZE)
    # check progress
    if i % display_step == 0 or (i+1) == TRAINING_ITERATIONS:

        train_accuracy = accuracy.eval(feed_dict={x: batch_xs,
                                                  y_: batch_ys,
                                                  keep_prob: 1.0})
        if(VALIDATION_SIZE):
            validation_accuracy = (
                accuracy.eval(
                    feed_dict={x: validation_images[0:BATCH_SIZE],
                               y_: validation_labels[0:BATCH_SIZE],
                               keep_prob: 1.0}))
            print('training_accuracy / validation_accuracy => %.2f / %.2f for step %d' % (train_accuracy, validation_accuracy, i))
            validation_accuracies.append(validation_accuracy)

        else:
            print('training_accuracy => %.4f for step %d' % (train_accuracy, i))
        train_accuracies.append(train_accuracy)
        x_range.append(i)

        # increase display_step
        if i % (display_step*10) == 0 and i:
            display_step *= 10
    # train on batch
    sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys, keep_prob: DROPOUT})

# check final accuracy on validation set
if(VALIDATION_SIZE):
    validation_accuracy = accuracy.eval(feed_dict={x: validation_images,
                                                   y_: validation_labels,
                                                   keep_prob: 1.0})
    print('validation_accuracy => %.4f' % validation_accuracy)
    plt.plot(x_range, train_accuracies, '-b', label='Training')
    plt.plot(x_range, validation_accuracies, '-g', label='Validation')
    plt.legend(loc='lower right', frameon=False)
    plt.ylim(ymax=1.1, ymin=0.0)
    plt.ylabel('accuracy')
    plt.xlabel('step')
    plt.show()

# read test data from CSV file
test_images = pd.read_csv('input/test.csv').values
test_images = test_images.astype(np.float)
test_images = np.multiply(test_images, 1.0 / 255.0)
print('test_images({0[0]},{0[1]})'.format(test_images.shape))
# using batches is more resource efficient
predicted_lables = np.zeros(test_images.shape[0])
for i in range(0, test_images.shape[0]//BATCH_SIZE):
    predicted_lables[i*BATCH_SIZE: (i+1)*BATCH_SIZE] = predict.eval(feed_dict={
        x: test_images[i*BATCH_SIZE: (i+1)*BATCH_SIZE],
        keep_prob: 1.0})

# save results
np.savetxt('output/tensorflow_CNN.csv',
           np.c_[range(1, len(test_images)+1), predicted_lables],
           delimiter=',',
           header='ImageId,Label',
           comments='',
           fmt='%d')
