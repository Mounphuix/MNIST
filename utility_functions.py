import numpy as np
import theano
import theano.tensor as T
import pandas as pd
from sklearn.cross_validation import train_test_split


def load_data(dataset):
    """
    Load training data
    """

    train_data = pd.read_csv(dataset)
    X = train_data.values[:, 1:].astype(float)
    y = train_data.values[:, 0].astype(int)

    (x_train,
     x_valid,
     y_train,
     y_valid) = train_test_split(X, y, test_size=0.25, random_state=1)

    valid_set_x, valid_set_y = shared_dataset((x_valid, y_valid))
    train_set_x, train_set_y = shared_dataset((x_train, y_train))
    rval = [(train_set_x, train_set_y), (valid_set_x, valid_set_y)]
    return rval


def shared_dataset(data_xy, borrow=True):
        """ Function that loads the dataset into shared variables
        """
        data_x, data_y = data_xy
        shared_x = theano.shared(np.asarray(data_x,
                                 dtype=theano.config.floatX),
                                 borrow=borrow)
        shared_y = theano.shared(np.asarray(data_y,
                                 dtype=theano.config.floatX),
                                 borrow=borrow)
        return shared_x, T.cast(shared_y, 'int32')
