import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cross_validation import cross_val_score
from sklearn.decomposition import PCA
from sklearn.svm import SVC


# loading training and testing data
print('Read training and testing data')
train_data = pd.read_csv('input/train.csv')
x_train = train_data.values[:, 1:].astype(float)
y_train = train_data.values[:, 0]
test_data = pd.read_csv('input/test.csv')
x_test = test_data.values.astype(float)

cv_scores = list()
cv_scores_std = list()

# determine the number of trees in classifier
print('Begin learning')
pca_components = [10, 15, 20, 25, 30, 40, 50]
for n in pca_components:
    print('n_components = %i' % (n))
    pca = PCA(n_components=n, whiten=True)
    pca.fit(x_train)
    x_train_pca = pca.transform(x_train)
    clf = SVC()
    cv_score = cross_val_score(clf, x_train_pca, y_train)
    cv_scores.append(np.mean(cv_score))
    cv_scores_std.append(np.std(cv_score))

scores = np.array(cv_scores)
stds = np.array(cv_scores_std)

# plot cross validation results
plt.plot(pca_components, scores)
plt.plot(pca_components, scores + stds, 'b--')
plt.plot(pca_components, scores - stds, 'b--')
plt.ylabel('cv score')
plt.xlabel('# of PCA components')
plt.savefig('cv_svc.png')
plt.title('PCA + SCV')
plt.show()

print('Make predictions')
# train classifier and predict using test data
pca = PCA(n_components=40, whiten=True)
pca.fit(x_train)
x_train_pca = pca.transform(x_train)
x_test_pca = pca.transform(x_test)

clf = SVC()
clf.fit(x_train_pca, y_train)
predictions = clf.predict(x_test_pca)
imageids = np.array(range(1, len(predictions)+1))
results = pd.DataFrame(data={'ImageId': imageids, 'Label': predictions})
results.to_csv('output/results_svc.csv', index=False)
